Feature: Homepage
  As a user I should see a donate block on the homepage.
  It should have a button with a label of $250.


Background:
  Given I am on "/"

Scenario:
  Then I should see "Donate!"

Scenario Outline:
  Then I should see "<amount>"

  Examples:
    |amount|
    |$25|
    |$50|
    |$100|
    |$250|

Scenario:
  I should be able to donate money!

  Then I press the "$100" button
  Then I should see "Please wait..."