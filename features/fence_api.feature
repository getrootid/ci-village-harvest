Feature: Remote Control Fence API
  In order to control fence security from anywhere
  As an API user
  I need to be able to POST JSON instructions to turn the fence on and off

  Scenario: List available products
    Given there are 5 products
    And I am on /admin
    When I click "Products"
    Then I should see 5 products

  Scenario: Add a new product
    Given I am on "admin/products"
    When I click "New Product"
    And I fill in "Name" with "Veloci-chew toy"
    And I fill in "Price" with "20"
    And I fill in "Description" with "Have your velociraptor chew on this instead!"
    And I press "Save"
    Then I should see "Product created FTW!"