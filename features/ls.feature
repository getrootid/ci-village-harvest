Feature: ls
  In order to see the directory structure
  As a UNIX user
  I need to be able to list the current directory's contents

  Background:
    Given there is a file named "John"

  Scenario: List 2 files in a directory
    Given there is a file named "Hammond"
    When I run "ls"
    Then I should see "John" in the output
    And I should see "Hammond" in the output

  Scenario: List 1 file and 1 directory
    Given there is a dir named "Ingen"
    When I run "ls"
    Then I should see "John" in the output
    And I should see "Ingen" in the output
