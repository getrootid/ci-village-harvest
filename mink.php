<?php

require __DIR__.'/vendor/autoload.php';

use Behat\Mink\Driver\GoutteDriver;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Session;

//$driver = new GoutteDriver();
$driver = new Selenium2Driver('firefox');

$session = new Session($driver);
$session->start();

$session->visit('http://jurassicpark.wikia.com');

var_dump($session->getStatusCode(),$session->getCurrentUrl());

//Document Element
$page = $session->getPage();

var_dump(substr($page->getText(),0, 75));

//Node Element
$header = $page->find('css','.wds-community-header__top-container .wds-community-header__sitename a');
var_dump($header->getText());

$nav = $page->find('css', '.wds-community-header__local-navigation');
$menuItem = $nav->find('css', 'li a');
var_dump($menuItem->getText());

$selectorHandler = $session->getSelectorsHandler();
$linkElement = $page->findLink('Community');
var_dump($linkElement->getAttribute('href'));

$fieldElement = $page->findField('wds-global-navigation__search-label');
var_dump($fieldElement->getAttribute('placeholder'));

$linkElement->click();
var_dump($session->getCurrentUrl());
$session->stop();